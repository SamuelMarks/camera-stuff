package com.example.opencvcamera;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2{

    private static String TAG = "MainActivity";
    JavaCameraView javaCameraView;
    Mat mRgba;
    ImageView result;
    boolean touched = false;
    BaseLoaderCallback mLoaderCallBack = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status){
                case BaseLoaderCallback.SUCCESS:{
                    javaCameraView.enableView();
                    break;
                }
                default:{
                    super.onManagerConnected(status);
                    break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        System.loadLibrary("opencv_java3");

        javaCameraView = (JavaCameraView)findViewById(R.id.java_camera_view);
        javaCameraView.setVisibility(SurfaceView.VISIBLE);
        javaCameraView.setCvCameraViewListener(this);


    }

    @Override
    protected void onPause(){
        super.onPause();
        if(javaCameraView!=null)
            javaCameraView.disableView();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if (javaCameraView!=null)
            javaCameraView.disableView();
    }

    @Override
    protected void onResume(){
        super.onResume();
        if (OpenCVLoader.initDebug()){
            Log.i(TAG, "Opencv loaded successfully");
            mLoaderCallBack.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        } else {
            Log.i(TAG, "Opencv not loaded");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this, mLoaderCallBack);
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(height, width, CvType.CV_8UC4);
    }

    @Override
    public void onCameraViewStopped() {
        mRgba.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();

        if(touched) {
            Intent myIntent = new Intent(this, image_display.class);
            startActivityForResult(myIntent, 0);
            //get rgb values from mRgba
            int col = mRgba.cols();
            int row = mRgba.rows();
            double redVal = 0;
            double greenVal = 0;
            double blueVal = 0;

            for (int y = 0; y < col; y++) {
                for (int x = 0; x < row; x++) {
                    double[] tmp = mRgba.get(x, y);
                    redVal += tmp[0];
                    greenVal += tmp[1];
                    blueVal += tmp[2];
                }
            }

            Log.i(TAG, "Color: "+ (int)redVal +","+ (int)greenVal +","+ (int)blueVal +"");

            TextView redText = findViewById(R.id.red);
            TextView blueText = findViewById(R.id.blue);
            TextView greenText = findViewById(R.id.green);

            redText.setText((int) redVal);
            blueText.setText((int) blueVal);
            greenText.setText((int) greenVal);

            touched = false;
        }

        return mRgba;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.i(TAG,"onTouch event");
        touched = true;
        return super.onTouchEvent(event);
    }

    public void goBack(){
        Intent newIntent = new Intent(this, MainActivity.class);
        startActivityForResult(newIntent, 0);
    }
}
