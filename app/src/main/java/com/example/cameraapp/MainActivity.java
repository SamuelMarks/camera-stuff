package com.example.cameraapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnCamera = (Button)findViewById(R.id.button);
        imageView = (ImageView)findViewById(R.id.imageView);

        btnCamera.setOnClickListener(new View.OnClickListener() {
           @Override
            public void onClick(View view){
               Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
               startActivityForResult(intent,0);
           }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = (Bitmap)data.getExtras().get("data");
        imageView.setImageBitmap(bitmap);
        Bitmap bitmap2 = ((BitmapDrawable)imageView.getDrawable()).getBitmap();

        // finds the red, blue and green values of the image
        int redValue;
        int blueValue;
        int greenValue;
        for (int y = 0; y < bitmap2.getHeight(); y++){
            for (int x = 0; x < bitmap2.getWidth(); x++){
                int pixel = bitmap.getPixel(x,y);
                redValue += Color.red(pixel);
                blueValue += Color.blue(pixel);
                greenValue += Color.green(pixel);
            }
        }

        // TO DO: trying to display the values on the screen for testing
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.editText);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);


        //code to get the RGB values of the image
//        Drawable drawable = imageView.getDrawable();
//        Rect bounds = drawable.getBounds();
//        int width = bounds.width();
//        int height = bounds.height();
//        int bitmapWidth = drawable.getIntrinsicWidth();
//        int bitmapHeight = drawable.getIntrinsicHeight();
//
//        for (int y = 0; y < bitmapHeight; y++){
//            for (int x = 0; x < bitmapWidth; x++){
//
//            }
//        }

    }

    //    public void takePhoto(View view){
//        //code to take photo
//    }


}
